<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model admin
		$this->load->model('admin');
	}

	public function index()
	{
		if($this->admin->logged_id())
		{
			//jika memang session sudah terdaftar, maka redirect ke halaman dashboard
			redirect("welcome");
		}else{
			//jika session belum terdaftar

			//maka set form validation
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');

			//set message form form validation
			$this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
				<div class="header"><b><i class="fa fa-exclamation-circle"></i>{field}</b>harus diisi</div>
				</div>');

			//cek validasi
			if($this->form_validation->run() == TRUE){

				//get data dari form
				$username = $this->input->post("username", TRUE);
				$password = MD5($this->input->post('password',TRUE));

				//checking data via model
				$checking = $this->admin->check_login('tbl_users', array('username' => $username), array('password' => $password));

				//jika ditemukan, maka cretae session
				if($checking != FALSE){
					foreach ($checking as $apps) {
						
						$session_data = array(
							'user_id'   => $apps->id_user,
							'user_name' => $apps->username,
							'user_pass' => $apps->password,
						);
						//set session userdata
						$this->session->set_userdata($session_data);
						redirect('welcome');
					}
				}else{
					$data['error'] = '<div class="header"><b><i class="fa fa-exclamation-circle"></i>ERROR</b> Username atau Password salah!</div></div>';
						$this->load->view('login',$data);
						}
				}else{
					$this->load->view('user/login');
				}
			}
		}

		public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}