<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();		
		
		$this->load->model('M_User');
	}


	function ambil_user(){

		$data['user'] = $this->M_User->get_user()->result();
		$this->load->view('user.php',$data);
	}

	function tambah_user(){
		$data['adduser'] = $this->M_User->tambah_user()->result();
		$this->load->view('user/form_user.php',$data);
	}

}

