<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assessment extends CI_Controller {

	public function index()
	{
		$query = $this->db->get('organisasi');  // Produces: SELECT * FROM mytable

		$data = [
			'pageTitle'=>'Assessment',
			'items'=>$query,
		];

		$this->load->view('assessment/index',$data);
	}

	public function build($id)
	{
		$query = $this->db->select('nama,id')->from('organisasi')->where('id', $id)->get();
		$elements = $this->db->select('nama_element,id')->from('elements')->get();
		$levels = $this->db->select('nama_level,id')->from('levels')->get();

		$kpa =  $this->db->select('judul_kpa,id')->from('ref_kpa')->get();

		$list = '';
		

		$data = [
			'pageTitle'=>'Assessment',
			'item'=>$query,
			'elements'=>$elements,
			'levels'=>$levels,
			'kpa'=>$kpa,
		];
		$this->load->view('assessment/build',$data);
	}

	public function organisasi($id)
	{
		$query = $this->db->select('nama,id')->from('organisasi')->where('id', $id)->get();
		

		

		$data = [
			'pageTitle'=>'Assessment',
			'item'=>$query,
			
		];
		$this->load->view('assessment/organisasi',$data);
	}

	public function build_save()
	{

		$data = array(
			'organisasi_id'=>$_POST['organisasi'],
			'element_id'=>$_POST['element'],
			'level_id'=>$_POST['level'],
			'kpa_id'=>$_POST['kpa'],
		);

		$this->db->insert('kpa_organisasi', $data);

	     redirect('assessment/build/'.$_POST['organisasi'], 'refresh');


	}

	public function update_pemenuhan()
	{
	// $kode = $s->id.'-'.$s->organisasi_id.'-'.$s->element_id.'-'.level_id.'-'.kpa_id.'-'.$s->daftar_uji_id;

		list($id,$organisasi_id,$element_id,$level_id,$kpa_id,$daftar_uji_id) = explode('-', $_POST['id']);

		$x = self::bingungNamanya($_POST['id']);




		$this->db->set('pemenuhan', $x);
		$this->db->where('id', $id);
		// $this->db->where('organisasi_id', $organisasi_id);
		// $this->db->where('element_id', $element_id);
		// $this->db->where('level_id', $level_id);
		// $this->db->where('kpa_id', $kpa_id);
		// $this->db->where('daftar_uji_id', $daftar_uji_id);
		$this->db->update('item_daftar_uji_kpa_organisasi');

		redirect('assessment/organisasi/'.$organisasi_id, 'refresh');
	}

	protected function bingungNamanya($id)
	{
		list($id,$organisasi_id,$element_id,$level_id,$kpa_id,$daftar_uji_id) = explode('-', $id);

		$query = $this->db->select('pemenuhan')->from('item_daftar_uji_kpa_organisasi')->where('id', $id)->get()->row();

		return $query->pemenuhan==1 ? 0 :1;

	}

	public function delete_build($id,$organisasi_id)
	{
		$this->db->where('id', $id);
		$this->db->delete('kpa_organisasi');
		redirect('assessment/build/'.$organisasi_id, 'refresh');
	}

	public function add_document($id,$organisasi_id)
	{
		$query =  $this->db->select('nama_uraian,id')->from('ref_daftar_uji')->get();

		$list = '';

		$data = [
			'pageTitle'=>'Assessment',
			'list'=>$query,
		];

		$this->load->view('assessment/add_doc',$data);

	}

	public function upload()
	{
		 //disini upload file
        $this->load->library('upload'); //panggil libary upload

        $extension = pathinfo($_FILES['file_doc']['name'], PATHINFO_EXTENSION);

        $namafile = "file" . '_' . time() . '.' . $extension; //nama file + fungsi time
        $config['upload_path'] = FCPATH . 'public/upload/'; //Folder untuk menyimpan hasil upload
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf|docx'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] = $namafile; //nama yang terupload nantinya

        $this->upload->initialize($config); //initialisasi upload dari array config
        $file_berkas = $this->upload->data();

        $this->upload->do_upload('file_doc');

        if ($extension != '') {
            $berkas = $file_berkas['file_name'];
        } else {
            $berkas = $this->input->post('old_berkas');
        }


        list($id,$organisasi_id,$element_id,$level_id,$kpa_id,$daftar_uji_id,$item_doc_daftar_uji) =  explode('-', $this->input->post('idnya'));

        $condition = [
			'id'=>$id,
			'organisasi_id'=>$organisasi_id,
			'element_id'=>$element_id,
			'level_id'=>$level_id,
			'kpa_id'=>$kpa_id,
			'daftar_uji_id'=>$daftar_uji_id,
			'item_doc_daftar_uji'=>$item_doc_daftar_uji,
        ];

        $data = [
        	    'file_doc' => $berkas,
        ];

        $this->db->where($condition);
        $this->db->update('item_daftar_uji_kpa_organisasi', $data);

        redirect('assessment/organisasi/'.$organisasi_id.'?element_id='.$element_id, 'refresh');
	}

	public function add_doc_save()
	{

		$data = array(
			'organisasi_id'=>$_POST['organisasi'],
			'element_id'=>$_POST['element'],
			'level_id'=>$_POST['level'],
			'kpa_id'=>$_POST['kpa'],
			'daftar_uji_id'=>$_POST['daftar_uji'],
			'kpa_organisasi_id'=>$_POST['kpa_organisasi'],
		);

		$this->db->insert('daftar_uji_kpa_organisasi', $data);

	     redirect('assessment/add_document/'.$_POST['kpa_organisasi'], 'refresh');


	}
	

	public function delete_ref_doc($id,$kpa_organisasi_id)
	{
		$this->db->where('id', $id);
		$this->db->delete('daftar_uji_kpa_organisasi');
		redirect('assessment/add_document/'.$kpa_organisasi_id, 'refresh');
	}

	public function add_item_document($id)
	{
		$query =  $this->db->select('nama_dokumen,id')->from('item_dokumen_daftar_uji')->get();

		$list = '';

		$data = [
			'pageTitle'=>'Assessment',
			'list'=>$query,
		];

		$this->load->view('assessment/add_item_doc',$data);

	}

	public function add_item_doc_save()
	{

		$data = array(
			'organisasi_id'=>$_POST['organisasi'],
			'element_id'=>$_POST['element'],
			'level_id'=>$_POST['level'],
			'kpa_id'=>$_POST['kpa'],
			'daftar_uji_id'=>$_POST['daftar_uji'],
			'kpa_organisasi_id'=>$_POST['kpa_organisasi'],
			'item_daftar_uji_id'=>$_POST['item_du'],
			'item_doc_daftar_uji'=>$_POST['item_daftar_uji'], // id daftar uji untuk relasi ke dokumen daftat uji
		);

		$this->db->insert('item_daftar_uji_kpa_organisasi', $data);

	     redirect('assessment/add_item_document/'.$_POST['item_du'], 'refresh');


	}

	public function delete_item_doc($id,$du_id)
	{
		$this->db->where('id', $id);
		$this->db->delete('item_daftar_uji_kpa_organisasi');
		redirect('assessment/add_item_document/'.$du_id, 'refresh');
	}

	
}
