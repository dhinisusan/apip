<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infapip extends CI_Controller {

	public function index()
	{
		$query = $this->db->get('organisasi');  // Produces: SELECT * FROM mytable

		$data = [
			'pageTitle'=>'Informasi APIP',
			'items'=>$query,
		];

		$this->load->view('infapip/index',$data);
	}
}
