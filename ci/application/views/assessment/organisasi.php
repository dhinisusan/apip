<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>
<?php $item = $item->row();
?>
<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
		<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Assessment <?= $item->id?></a>
						</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>
				<div class="page-header">

					<h1 style="margin-left: 20px;">
						Assessment
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							build assessment
						</small>
					</h1>
				</div>

				<div class="page-content">

					<p>
					Nama Instansi :
					</br>
					Tahun :
					</p>

					<nav class="navbar">

						<ul class="nav navbar-nav">
							<li class="active"><a href="<?= site_url('assessment/organisasi/'.$this->uri->segment(3).'?mod=home')?>"><i class="fa fa-home"></i> Home</a></li>
							<?php for($i = 1;$i<=6;$i++): ?>
								<li><a href="<?= site_url('assessment/organisasi/'.$this->uri->segment(3).'?element_id='.$i) ?>">Element <?=$i?></a></li>					
							<?php endfor ?>
							<li class="active"><a href="<?= site_url('assessment/organisasi/'.$this->uri->segment(3)).'?mod=kesimpulan'?>"><i class="fa fa-angle-double-right"></i> Simpulan</a></li>
							
						</ul>
					</nav>

					<div class="row">
						<?php 

						if(isset($_GET['element_id'])):


							$sql = "SELECT DISTINCT level_id from  kpa_organisasi where organisasi_id=".$this->uri->segment(3).' AND element_id='.@$_GET['element_id'];
							$query = $this->db->query($sql);						



							foreach($query->result() as $row):

								$html = "<div class='col-md-12'>";

								$html .="<h4> Level".$row->level_id."</h4>";
								$html .="</div>";

								$skuel ="SELECT item_daftar_uji_kpa_organisasi.*,judul_kpa,nama_uraian,nama_dokumen,pemenuhan
								FROM `item_daftar_uji_kpa_organisasi`
								JOIN ref_kpa on item_daftar_uji_kpa_organisasi.kpa_id = ref_kpa.id
								JOIN ref_daftar_uji on item_daftar_uji_kpa_organisasi.daftar_uji_id = ref_daftar_uji.id
								JOIN item_dokumen_daftar_uji on item_daftar_uji_kpa_organisasi.item_doc_daftar_uji = item_dokumen_daftar_uji.id
								WHERE organisasi_id = ".$this->uri->segment(3)." AND element_id=".@$_GET['element_id']." AND level_id =".$row->level_id."
								ORDER by level_id ASC";

								// echo $skuel;

								$qry = $this->db->query($skuel);

						//print_r($skuel);

								$html.="<table class='table table-bordered'>
								<thead>
								<tr>
								<th>KPA</th>
								<th>Daftar Uji</th>
								<th>Uraian</th>
								<th>Pemenuhan</th>
								<th>File Upload</th>
								<th>Aksi</th>
								</tr>
								</thead>";

								$judul ='';
								$judul_uji ='';

								foreach($qry->result() as $s):

									$title = $s->judul_kpa != $judul ? $s->judul_kpa : '';
									$title_uji = $s->nama_uraian != $judul_uji ? $s->nama_uraian : '';

									$kode = $s->id.'-'.$s->organisasi_id.'-'.$s->element_id.'-'.$s->level_id.'-'.$s->kpa_id.'-'.$s->daftar_uji_id;

									$kodenya = $s->id.'-'.$s->organisasi_id.'-'.$s->element_id.'-'.$s->level_id.'-'.$s->kpa_id.'-'.$s->daftar_uji_id.'-'.$s->item_doc_daftar_uji;

									$html.="<form method='post' action='".site_url('assessment/upload')."' enctype='multipart/form-data'>
									<input name='idnya' type='hidden' value='$kodenya'>
									<input type='hidden' name='old_berkas' value='".$s->file_doc."'>";


									$html.="<tr><th>".$title."</th><th><label class='pull-right inline'>".$title_uji."</th><th><label class='pull-right inline'>".$s->nama_dokumen."</th>";
									if($s->nama_uraian != $judul_uji){




										$checked = $s->pemenuhan != 0 ? 'checked' : '';
										$html.="<th><label class='pull-right inline'>
										<small class='muted smaller-90'>Sudah terpenuhi?</small>
										<input id='id-button-borders'  type='checkbox' class='status_checks ace ace-switch ace-switch-5 btn ". $s->pemenuhan = 0 ? "btn-secondary" : "btn-primary" ."' data-id='$kode' ".$checked."/>
										<span class='lbl middle'></span>
										</label></th>";
									} else {
										$html.="<th>&nbsp;</th>";
									}

									if ($s->file_doc !=null) {
										$html.="<th><label class='pull-right inline'><input type='file' name='file_doc' /></th><th width='15%'>
										<div class='btn-group'>
										<a href='".base_url('public/upload/'.$s->file_doc)."' class='btn btn-success' target='_blank'><i class='fa fa-eye'></i></a>
										<button class='btn btn-info'>
										<i class='ace-icon fa fa-cloud-upload  align-top bigger-125 icon-on-right'></i>
										</button>
										</div>
										</th>";
									} else{
										$html.="<th><label class='pull-right inline'><input type='file' name='file_doc' /></th><th><button class='btn btn-info'>
										<i class='ace-icon fa fa-cloud-upload  align-top bigger-125 icon-on-right'></i>
										</button></th>";

									}

									$html.="</tr></form>";


									$judul = $s->judul_kpa;
									$judul_uji = $s->nama_uraian;
								endforeach;	

								$html.="</table>";



								echo $html;



							endforeach;?>
							

						<?php endif; ?>

						<?php switch (@$_GET['mod']) {
							case 'home':
							echo '<div class="col-md-12">
							<div class="alert alert-danger">

							Panduan Sistem Informasi Daftar Uji dan Dokumen Pendukung Penjaminan Kualitas Mengacu Panduan Praktis Perumusan Permaalahan, Penyebab dan Saran untuk Peningkatan Kapabilitas APIP.
							</div>
							</div>';
							break;
							case 'kesimpulan': ?>
							<div class="container">
								<div class="row" style="margin-top: 30px;">
									<div class="col-md-10">
										<table id="dynamic-table" class="table table-striped table-bordered table-hover">

										<thead>
											<tr>
												<th width="20%"></th>
												<th>Pemenuhan Elemen Level 1</th>
												<th>Pemenuhan Elemen Level 2</th>
												<th>Pemenuhan Elemen Level 3</th>
												<th>Pemenuhan Elemen Level 4	</th>
												<th>Pemenuhan Elemen Level 5	</th>
												<th>Pemenuhan Elemen Level 6	</th>
												<th>Simpulan</th>
											</tr>
										</thead>

										<tbody>
											<?php for($i=1;$i<=6;$i++):?>
											<tr>
											<td>
												Elemen <?= $i ?>
											</td>
											<?php for($lvl=1;$lvl<=6;$lvl++):?>
											<td>

												<?php

													$alldu = $this->db->query("SELECT count( DISTINCT daftar_uji_id) AS alldu FROM item_daftar_uji_kpa_organisasi WHERE organisasi_id=".$this->uri->segment(3)." AND element_id=".$i." AND level_id=".$lvl)->row()->alldu;

													//untuk yang sudah ke isi

													$duyes = $this->db->query("SELECT count( DISTINCT daftar_uji_id) AS duyes FROM item_daftar_uji_kpa_organisasi WHERE organisasi_id=".$this->uri->segment(3)." AND element_id=".$i." AND pemenuhan=1 AND level_id=".$lvl)->row()->duyes;

													if ($alldu==0 ) {
														$isian = 0;
													}

													else if ($duyes < $alldu ) {
														$isian = 1;
													} else{
														$isian=2;
													}

												 ?>
												<?= $isian ?>
										
											</td>
											<?php endfor?>
											</tr>
											<?php endfor?>										
										</tbody>
										
									</table>	
								</div>

							</div>
						</div>
						<?php	break;

					} ?>

				</div>


			</div><!-- /.page-content -->
			<div class="page-content">



			</div>
		</div>
	</div><!-- /.main-content -->

	<?php $this->load->view("layouts/_partials/footer.php") ?>

</div><!-- /.main-container -->

<!-- basic scripts -->



<?php $this->load->view("layouts/_partials/scripts.php") ?>
<script type="text/javascript">
	$(document).on('click','.status_checks',function()
	{ 
		var status = ($(this).prop(':checked')) ? '0' : '1'; 
// var msg = (status=='0')? 'Deactivate' : 'Activate'; 
if(confirm("Apakah anda yakin dokumen telah terpenuhi ?"))
{ 
	var current_element = $(this); 
	var id = $(current_element).data('id');
	url = "<?php echo base_url().'/assessment/update_pemenuhan'?>"; 
	$.ajax({
		type:"POST",
		url: url, 
		data: {"id":id,"status":status}, 
		success: function(data) { 
			location.reload();
		} });
}  
});
</script>
</body>
</html>
