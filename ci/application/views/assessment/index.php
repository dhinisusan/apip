<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>

<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
		<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Dashboard</a>
						</li>
						<li class="active">Assessment</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>

				<div class="page-header">

					<h1 style="margin-left: 20px;">
						Assessment
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							overview &amp; stats
						</small>
					</h1>
				</div>

				<div class="page-content">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center">
									<label class="pos-rel">
										#
										<span class="lbl"></span>
									</label>
								</th>
								<th class="text-center">PEMERINTAH DAERAH</th>

								<!-- <th class="hidden-480">Status</th> -->

								<th class="text-center"> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no=1;
							foreach($items->result() as $item):?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td>
										<a href="<?=site_url('assessment/organisasi/'.$item->id.'?mod=home')?>"><?=$item->nama?></a>		
									</td>
									<!-- <td>masih bingung aing</td> -->
									<td class="text-center">
										<a href="<?=site_url('assessment/build/'.$item->id)?>" class="tooltip-info" data-toggle="tooltip" title="Build Datfar Uji">
											<span class="yellow">
												<i class="ace-icon fa fa-gears bigger-120"></i>
											</span>
										</a>
										<a href="#" class="tooltip-info" data-toggle="tooltip" title="View">
											<span class="blue">
												<i class="ace-icon fa fa-search-plus bigger-120"></i>
											</span>
										</a>

										<a href="#" class="tooltip-success" data-toggle="tooltip" title="Edit">
											<span class="green">
												<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
											</span>
										</a>
										<a href="#" class="tooltip-error" data-toggle="tooltip" title="Delete">
											<span class="red">
												<i class="ace-icon fa fa-trash-o bigger-120"></i>
											</span>
										</a>
									</td>
								</tr>
							<?php endforeach?>
						</tbody>
					</table>
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->

		<?php $this->load->view("layouts/_partials/footer.php") ?>

	</div><!-- /.main-container -->

	<!-- basic scripts -->

	

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>
