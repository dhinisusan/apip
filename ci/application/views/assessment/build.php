<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>
<?php $item = $item->row();
 ?>
<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Assessment <?= $item->id?></a>
						</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>
				<div class="page-header">

					<h1 style="margin-left: 20px;">
								Assessment
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									build assessment
								</small>
							</h1>
						</div>

				<div class="page-content">

					<div class="alert alert-block alert-danger">
									
									<i class="ace-icon fa fa-warning red"></i>
									<strong class="green">
										HATI -HATI DALAM MEMBUAT  DAFTAR UJI ELEMENTS 
									</strong>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h3><?= $item->nama?></h3>	
									</div>
									<div class="col-md-12">
										<form action="<?= site_url('assessment/build_save')?>" method="post">
											<input type="hidden" name="organisasi" value="<?=$item->id?>">
											<div class="form-group">
												<label>Pilih Element</label>
												<select name="element" class="form-control select2">
													<?php  foreach($elements->result() as $elm): ?>
													<option value="<?=$elm->id?>"><?=$elm->nama_element?></option>
													<?php endforeach?>
												</select>
											</div>
											<div class="form-group">
												<label>Pilih Level</label>
												<select name="level" class="form-control select2">
													<?php  foreach($levels->result() as $lvl): ?>
													<option value="<?=$lvl->id?>"><?=$lvl->nama_level?></option>
													<?php endforeach?>
												</select>
											</div>
											<div class="form-group">
												<label>Pilih KPA</label>
												<select name="kpa" class="form-control select2">
													<?php  foreach($kpa->result() as $thing): ?>
													<option value="<?=$thing->id?>"><?=$thing->judul_kpa?></option>
													<?php endforeach?>
												</select>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-block btn-info"> Simpan</button>
											</div>
										</form>
										
									</div>
								</div>

								
				</div><!-- /.page-content -->
				<div class="page-content">
					<?php
					$sql = "SELECT kpa_organisasi.id, organisasi.nama as nama_organisasi, elements.nama_element as nama_element, levels.nama_level as nama_level, ref_kpa.judul_kpa as nama_kpa,organisasi_id FROM `kpa_organisasi` JOIN organisasi on organisasi.id = kpa_organisasi.organisasi_id JOIN elements on elements.id = kpa_organisasi.element_id JOIN levels on levels.id = kpa_organisasi.level_id JOIN ref_kpa ON ref_kpa.id = kpa_organisasi.kpa_id where organisasi_id = ".$this->uri->segment(3)." ORDER BY element_id ASC";
					$query = $this->db->query($sql);

					 ?>
					<table class="table table-bordered">
									<thead>
										<tr>
											<th>Element</th>
											<th>Level</th>
											<th>KPA</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										if ($query->num_rows() > 0):
										foreach($query->result() as $row):
											?>
										<tr>
											<td><?=$row->nama_element?></td>
											<td><?=$row->nama_level?></td>
											<td><?=$row->nama_kpa?></td>
											<td>
												<a href="<?= site_url('assessment/add_document/'.$row->id.'/'.$row->organisasi_id)?>" data-toggle="tooltip" title="mapping dokumen uji"><i class="fa fa-copy"></i></a>
												<a href="<?= site_url('assessment/delete_build/'.$row->id.'/'.$row->organisasi_id)?>" onclick="return confirm('Apakah Anda Yakin menghapus data ini?')" data-toggle="tooltip" title="hapus"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
										<?php

										endforeach;
										endif;

										?>
									</tbody>
								</table>
					
				</div>
			</div>
		</div><!-- /.main-content -->

		<?php $this->load->view("layouts/_partials/footer.php") ?>

	</div><!-- /.main-container -->

	<!-- basic scripts -->

	

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>
