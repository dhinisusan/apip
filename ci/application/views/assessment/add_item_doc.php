<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>
<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Assessment</a>
						</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>
				<div class="page-header">

					<h1 style="margin-left: 20px;">
								Assessment
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									add document assessment
								</small>
							</h1>
						</div>

				<div class="page-content">

					<div class="alert alert-block alert-danger">
									
									<i class="ace-icon fa fa-warning red"></i>
									<strong class="green">
										HATI -HATI DALAM MEMBUAT  DAFTAR UJI ELEMENTS 
									</strong>
								</div>
								<div class="row">

									<?php
$sqlDaftarUji = "SELECT du.id,
organisasi.nama as nama_organisasi,
elements.nama_element,
levels.nama_level,
ref_kpa.judul_kpa as nama_kpa,
ref_daftar_uji.nama_uraian,
kpa_organisasi_id,
kpa_id,
organisasi_id,
level_id,
element_id,
daftar_uji_id
FROM daftar_uji_kpa_organisasi as du
JOIN organisasi on organisasi.id = du.organisasi_id
JOIN elements on elements.id = du.element_id
JOIN levels on levels.id = du.level_id
JOIN ref_kpa on ref_kpa.id = du.kpa_id
JOIN ref_daftar_uji on ref_daftar_uji.id = du.daftar_uji_id
where du.id =". $this->uri->segment(3);

					$queryDU = $this->db->query($sqlDaftarUji);
					$item = $queryDU->row();

					 ?>
									<div class="col-md-7">
										<h3><?= $item->nama_organisasi?></h3>	
										<table class="table">
											<tr>
												<td width="30%"><b>Element</b></td>
												<td><?=$item->nama_element?></td>
											</tr>
											<tr>
												<td width="30%"><b>Level</b></td>
												<td><?=$item->nama_level?></td>
											</tr>
											<tr>
												<td width="30%"><b>Refrensi KPA</b></td>
												<td><?=$item->nama_kpa?></td>
											</tr>
											<tr>
												<td width="30%"><b>Refrensi Daftar Uji</b></td>
												<td><?=$item->nama_uraian?></td>
											</tr>
										</table>
									</div>
									<div class="col-md-12">
										<form action="<?= site_url('assessment/add_item_doc_save')?>" method="post">
											<input type="hidden" name="organisasi" value="<?=$item->organisasi_id?>">
											<input type="hidden" name="element" value="<?=$item->element_id?>">
											<input type="hidden" name="level" value="<?=$item->level_id?>">
											<input type="hidden" name="kpa" value="<?=$item->kpa_id?>">
											<input type="hidden" name="daftar_uji" value="<?=$item->daftar_uji_id?>">
											<input type="hidden" name="kpa_organisasi" value="<?=$item->kpa_organisasi_id?>">
											<input type="hidden" name="item_du" value="<?=$item->id?>">
											<div class="form-group">
												<label>Pilih Daftar Dokumen Uji</label>
												<select name="item_daftar_uji" class="form-control select2">
													<?php  foreach($list->result() as $elm): ?>
													<option value="<?=$elm->id?>"><?=$elm->nama_dokumen?></option>
													<?php endforeach?>
												</select>
											</div>

											<div class="form-group">
												<button type="submit" class="btn btn-block btn-info"> Simpan</button>
											</div>
											
										</form>
										
									</div>
								</div>

								
				</div><!-- /.page-content -->
				<div class="page-content">
					

					<?php
					$sqlItem = "SELECT item_daftar_uji_kpa_organisasi.id, nama_dokumen FROM `item_daftar_uji_kpa_organisasi` JOIN item_dokumen_daftar_uji on item_daftar_uji_kpa_organisasi.item_doc_daftar_uji = item_dokumen_daftar_uji.id
					where daftar_uji_id=".$item->daftar_uji_id;
					
					$queryItem = $this->db->query($sqlItem);

					 ?>

					 <table class="table table-bordered">
									<thead>
										<tr>
											<th>Daftar Uji</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										if ($queryItem->num_rows() > 0):
										foreach($queryItem->result() as $row):
											?>
										<tr>
											<td><?=$row->nama_dokumen?></td>
											<td>
												
												<a href="<?= site_url('assessment/delete_item_doc/'.$row->id.'/'.$item->id)?>" onclick="return confirm('Apakah Anda Yakin menghapus data ini?')" data-toggle="tooltip" title="hapus"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
										<?php

										endforeach;
										endif;

										?>
									</tbody>
								</table>

				</div>
			</div>
		</div><!-- /.main-content -->

		<?php $this->load->view("layouts/_partials/footer.php") ?>

	</div><!-- /.main-container -->

	<!-- basic scripts -->

	

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>
