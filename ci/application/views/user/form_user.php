<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>

<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
		<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Dashboard</a>
						</li>
						<li class="active">Tambah User</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>

				
				
				<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<form class="form-horizontal" role="form">
								</br>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Usename </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Password </label>

										<div class="col-sm-9">
											<input type="password" id="form-field-2" placeholder="Password" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									

									

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-tags">Role</label>

										<div class="col-sm-9">
											<div class="inline">
												<input type="text" name="tags" id="form-field-tags" value="Tag Input Control" placeholder="Enter tags ..." />
											</div>
										</div>
									</div>

									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="button">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
									
								</form>		

								

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->

		<?php $this->load->view("layouts/_partials/footer.php") ?>

	</div><!-- /.main-container -->

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>
