<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>

<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
		<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Dashboard</a>
						</li>
						<li class="active">Management User</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>

				

				<div class="page-content">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center">
									<label class="pos-rel">
										#
										<span class="lbl"></span>
									</label>
								</th>
								<th class="text-center">Username</th>

								<th class="hidden-480">Level</th>

								<th class="text-center"> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<a href="#" id="id-btn-dialog2" class="btn btn-info btn-sm">Tambah User</a></br></br>
							

							<?php 
							$no=1;
							foreach($user as $u){ ?>
							
									

								<tr>
									<td class="text-center"><?=$no++?></td>
									<td class="text-center"><?php echo $u->nama_user ?></td>
									
									<td>
										<a href=""><?php echo $u->role ?></a>

									</td>
									<td class="text-center">
										
										<a href="#" class="tooltip-info" data-toggle="tooltip" title="View">
											<span class="blue">
												<i class="ace-icon fa fa-search-plus bigger-120"></i>
											</span>
										</a>

										<a href="#" class="tooltip-success" data-toggle="tooltip" title="Edit">
											<span class="green">
												<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
											</span>
										</a>
										<a href="#" class="tooltip-error" data-toggle="tooltip" title="Delete">
											<span class="red">
												<i class="ace-icon fa fa-trash-o bigger-120"></i>
											</span>
										</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->

		<?php $this->load->view("layouts/_partials/footer.php") ?>

	</div><!-- /.main-container -->

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>
