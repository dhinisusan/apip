<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>

<body class="no-skin">

<?php $this->load->view("layouts/_partials/navbar.php") ?>
	<div class="main-container ace-save-state" id="main-container">

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-lightbulb-o white"></i>
									<span class="red">LAYANAN</span>
									<span class="red" id="id-text2">APIP</span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; Perwakilan BPKP Sulteng</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>
							 <?php if(isset($error)) { echo $error; }; ?>
											<form class="form-signin" method="POST" action="<?php echo base_url() ?>index.php/login">

												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" name="username" placeholder="Username" />
															<?php echo form_error('username'); ?>
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" name="password" placeholder="Password" />
															<?php echo form_error('password'); ?>
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" />
															<span class="lbl"> Remember Me</span>
														</label>
														
														<button type="submit" name="btn-login" id="btn-login" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>					
														</button>
														</a>
													</div>
													
												</fieldset>
											</form>
										</div><!-- /.widget-main -->
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->					
							</div><!-- /.position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
	</div><!-- /.main-container -->

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>