<!--[if !IE]> -->
	<script src="<?= base_url()?>public/assets/js/jquery-2.1.4.min.js"></script>

	<!-- <![endif]-->

		<!--[if IE]>
<script src="<?= base_url()?>public/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url()?>public/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url()?>public/assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url()?>public/assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url()?>public/assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>public/assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url()?>public/assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url()?>public/assets/js/buttons.print.min.js"></script>
<script src="<?= base_url()?>public/assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url()?>public/assets/js/dataTables.select.min.js"></script>

<script src="<?= base_url()?>public/assets/js/select2.min.js"></script>

<!-- ace scripts -->
<script src="<?= base_url()?>public/assets/js/ace-elements.min.js"></script>
<script src="<?= base_url()?>public/assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->

<script type="text/javascript">
	$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('.select2').select2();
});
</script>