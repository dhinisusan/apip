<div id="sidebar" class="sidebar responsive ace-save-state">
			<script type="text/javascript">
				try{ace.settings.loadState('sidebar')}catch(e){}
			</script>

			<div class="sidebar-shortcuts" id="sidebar-shortcuts">
				<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
					
				</div>

				<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
					<span class="btn btn-success"></span>

					<span class="btn btn-info"></span>

					<span class="btn btn-warning"></span>

					<span class="btn btn-danger"></span>
				</div>
			</div><!-- /.sidebar-shortcuts -->

			<ul class="nav nav-list">
				<li class="">
					<a href="<?=site_url('/')?>">
						<i class="menu-icon fa fa-tachometer"></i>
						<span class="menu-text"> Dashboard </span>
					</a>

					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="<?=site_url('/infapip')?>">
						<i class="menu-icon fa fa-pencil-square-o"></i>
						<span class="menu-text"> Informasi Apip </span>
					</a>

					<b class="arrow"></b>
				</li>
				<li>
					<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text">
								Penilaian Mandiri
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="<?= site_url('assessment')?>">
									<i class="menu-icon fa fa-caret-right"></i>

									Dokumen Penilaian
									<b class="arrow fa fa-caret-right"></b>
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="">
					<a href="<?=site_url('user/ambil_user')?>">
						<i class="menu-icon fa fa-pencil-square-o"></i>
						<span class="menu-text"> Manajemen User </span>
					</a>

					<b class="arrow"></b>
				</li>

			</ul><!-- /.nav-list -->

			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>