<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("layouts/_partials/head.php") ?>
	<!-- custom css di bawah sini-->
</head>

<body class="no-skin">
	<?php $this->load->view("layouts/_partials/navbar.php") ?>

	<div class="main-container ace-save-state" id="main-container">
		
<?php $this->load->view("layouts/_partials/sidebar.php") ?>
		

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Dashboard</a>
						</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						
					</div><!-- /.nav-search -->
				</div>
				<div class="page-header">

					<h1 style="margin-left: 20px;">
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div>

				<div class="page-content">

					<div class="alert alert-block alert-success">
									<button type="button" class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>

									<i class="ace-icon fa fa-check green"></i>

									Selamat Datang di 
									<strong class="green">
										Aplikasi layanan
										<small>(1.0)</small>
									</strong>,
								Daftar Uji dan Dokumen Pendukung Penjaminan Kualitas Mengacu Panduan Praktis Perumusan Permaalahan, Penyebab dan Saran untuk Peningkatan Kapabilitas APIP.
								</div>
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->

		<?php $this->load->view("layouts/_partials/footer.php") ?>

	</div><!-- /.main-container -->

	<!-- basic scripts -->

	

	<?php $this->load->view("layouts/_partials/scripts.php") ?>

</body>
</html>
