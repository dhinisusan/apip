-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2019 at 05:23 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apip`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar_uji_kpa_organisasi`
--

CREATE TABLE `daftar_uji_kpa_organisasi` (
  `id` int(11) NOT NULL,
  `organisasi_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `kpa_id` int(11) NOT NULL,
  `daftar_uji_id` int(11) NOT NULL,
  `kpa_organisasi_id` int(11) NOT NULL,
  `file_doc` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `daftar_uji_kpa_organisasi`
--

INSERT INTO `daftar_uji_kpa_organisasi` (`id`, `organisasi_id`, `element_id`, `level_id`, `kpa_id`, `daftar_uji_id`, `kpa_organisasi_id`, `file_doc`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 1, 2, 3, 1, NULL, NULL, NULL),
(4, 1, 1, 1, 2, 68, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE `elements` (
  `id` int(11) NOT NULL,
  `nama_element` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `nama_element`, `created_at`, `updated_at`) VALUES
(1, 'Element 1', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(2, 'Element 2', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(3, 'Element 3', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(4, 'Element 4', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(5, 'Element 5', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(6, 'Element 6', '2019-10-01 17:00:00', '2019-10-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `item_daftar_uji_kpa_organisasi`
--

CREATE TABLE `item_daftar_uji_kpa_organisasi` (
  `id` int(11) NOT NULL,
  `organisasi_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `kpa_id` int(11) NOT NULL,
  `daftar_uji_id` int(11) NOT NULL,
  `item_daftar_uji_id` int(11) NOT NULL,
  `kpa_organisasi_id` int(11) NOT NULL,
  `item_doc_daftar_uji` int(11) NOT NULL,
  `pemenuhan` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item_dokumen_daftar_uji`
--

CREATE TABLE `item_dokumen_daftar_uji` (
  `id` int(11) NOT NULL,
  `nama_dokumen` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `item_dokumen_daftar_uji`
--

INSERT INTO `item_dokumen_daftar_uji` (`id`, `nama_dokumen`, `created_at`, `updated_at`) VALUES
(1, 'IAC (memuat mandat untuk melaksanakan audit ketaatan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'PKPT (memuat Penugasan Audit Ketaatan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Pedoman/SOP audit ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Surat Tugas ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'PKA Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'KKA Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Bukti Reviu Berjenjang Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Notisi Audit Ketaatan/Pokok-Pokok Permasalahan Hasil Pemeriksaan (P2HP)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Laporan Hasil Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Bukti Tindak Lanjut Hasil Audit Ketaatan/ Berita Acara Kesepakatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Hasil wawancara dengan auditi/stakeholders', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'IAC memuat mandat audit kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'PKPT memuat Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Pedoman/SOP Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Surat Tugas ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'PKA Audit Kinerja dan  kegiatan pengawasan lain', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'KKA Audit Kinerja dan  kegiatan pengawasan lain', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Routing Slip/ Bukti Reviu Berjenjang Audit Kinerja dan  kegiatan pengawasan lain', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Notisi Audit Kinerja dan  kegiatan pengawasan lain/Pokok-Pokok Permasalahan Hasil Pemeriksaan (P2HP)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Laporan Hasil Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Bukti Tindak Lanjut Hasil Pengawasan Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan) /Berita Acara Kesepakatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Hasil Wawancara dengan Stakeholders', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'IAC (memuat mandat memberikan jasa advis)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'PKPT (memuat Layanan Jasa Advis)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Pedoman/SOP Layanan Jasa Advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Surat Permintaan Jasa Advis/ToR/ Kesepakatan/Inisiatif PKPT APIP, atau', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Surat Tugas Layanan Jasa Advis (mencantumkan pernyataan independensi)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Buku Tamu (Layanan Coaching Clinic)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Kertas Kerja/ Dokumentasi Layanan/ Laporan Berkala Jasa Advis (Dokumentasi hasil konsultansi)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Laporan Hasil Layanan Jasa Advis per penugasan ataupun periodik', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Hasil Wawancara dengan auditi /Stakeholders/Hasil Survei Kepuasan Pengguna Layanan Advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'PKPT/PKAT atau rincian tugas pengawasan yang harus dilaksanakan APIP (Misalnya audit operasional, audit reguler, audit keuangan, reviu RKPD, reviu LKJ, Reviu Lapkeu, audit tujuan tertentu dsb.)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Rincian kompetensi yang dibutuhkan untuk melaksanakan tugas pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Peraturan Gubernur/Bupati/Walikota tentang Uraian Jabatan atau SOTK (berisi uraian pekerjaan, persyaratan kinerja dan persyaratan jabatan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'SK/peraturan Gubernur/Bupati/Walikota tentang Klasifikasi Tunjangan setiap Jabatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'SOP/kebijakan rekrutmen pegawai (JFA maupun fungsional yang lain)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Surat Permintaan Formasi CPNS kepada BKD/MenPAN-RB atau Penerimaan pegawai dari unit lain dengan kompetensi yang sesuai', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Usulan Diklat Pembentukan Auditor (dari CPNS, perpindahan maupun inpassing dari jabatan lain)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Laporan realisasi diklat pembentukan atau laporan pemenuhan kebutuhan pegawai', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Wawancara dengan auditan terkait kualitas / kompetensi SDM APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Jam pelatihan minimal bagi auditor (dapat mengacu PermenPAN-RB Nomor Per/220/M.PAN/7/2008 tanggal 4 Juli 2008 tentang Jabatan Fungsional Auditor dan Angka Kreditnya) dan atau standar audit AAIPI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Rencana/usulan peserta diklat/PPM, ST Diklat/ PPM, dan Sertifikat Diklat/PPM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'SE Inspektur tentang dukungan menjadi anggota profesi atau Kartu Anggota Profesi, misal AAIPI', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Laporan realisasi pengembangan Profesi/Diklat/PKS/PPM bagi Pejabat Fungsional di lingkungan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'Audit Universe atau usulan PKPT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Peta Kompetensi SDM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Hasil perhitungan kebutuhan sumber daya pengawasan dibandingkan dengan jumlah dan lingkup pengawasan yang mampu dilaksanakan oleh SDM APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'PKPT/PKAT yang mencantumkan rincian distribusi Personil untuk setiap penugasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'SK Inspektur tentang pembentukan satgas/ penugasan antar Irban, atau SK Inspektur tentang Audit bersama APIP lain/ Join Audit', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'SK Inspektur tentang Pelaksanaan Penugasan oleh Pihak Lain (Outsourcing)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Bukti implementasi atas Kebijakan dan Strategi Pemenuhan SDM Pengawasan (Surat Tugas dan Laporan Hasil Kegiatan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'SK Inspektur tentang Kerangka Kompetensi Pejabat Fungsional Pengawasan. ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Rencana Diklat Substansi SDM APIP (berisi diklat teknis substansi seperti diklat audit kinerja, jasa konsultansi, manajemen risiko, PPBR)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Bukti realisasi Diklat Subtansi (misalnya Surat Tugas, Sertifikat, dan Laporan kegiatan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Rencana Diklat Penjenjangan Fungsional Auditor dan fungsional lainnya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Realisasi Diklat Penjenjangan Fungsional Auditor dan fungsional lainnya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Program perolehan sertifikasi profesi internal auditor dalam DPA, atau pemberian dukungan kepada auditor untuk memperoleh sertifikasi profesi internal auditor dengan biaya di luar DPA (seperti: SE Dukungan Perolehan Sertifikasi Internal Auditor)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Realisasi dari program perolehan sertifikasi profesi internal auditor (seperti: Usulan Sertifikasi profesi, ST Sertifikasi Internal Auditor, Sertifikat Profesi Internal Auditor, Laporan Kegiatan Mengikuti Sertifikasi Profesi Internal Auditor)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Surat Tugas Pengawasan, misal Audit Kinerja', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Bukti Kompetensi audit kinerja yang dimiliki oleh Tim Audit (diperoleh dari data Peta Kompetensi atau Sertifikat yang dimiliki)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Dokumen SKI untuk seluruh staf', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Angka kredit untuk Jabatan Fungsional Auditor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Kebijakan Pola Karir di lingkungan APIP (meliputi rotasi, promosi dan mutasi antar Irban, didasarkan pada kompetensi dan kinerja, bisa mengacu ke Perka BPKP dan Permenpan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Bukti pelaksanaan kebijakan pola karir (SK rotasi atau promosi atau mutasi antar Irban)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'SK Inspektur pemberian penghargaan terhadap pegawai berkinerja baik atau pelaksanaan Pemberian Penghargaan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Kebijakan Mengenai Kriteria Perilaku dan Praktik Kerjasama Tim yang Efektif (seperti: SE khusus tentang kriteria perilaku dan praktik kerjasama tim, atau kebijakan lain yang di dalamnya mengatur tentang kriteria perilaku dan praktik kerjasama tim)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Rencana Diklat Penjenjangan Auditor Muda (KT), Madya (PT)/PKS/ PPM atau Workshop, pemagangan, Joint Audit, kegiatan lain yang di dalamnya terdapat materi kepemimpinan, komunikasi dan hubunga kerja yang baik', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Pelaksanaan dari rencana kegiatan  di atas (Kegiatan yang mampu memberikan kesempatan pengembangan kepemimpinan, kerja sama tim, komunikasi atau hubungan kerja yang baik, seperti Pelatihan Budaya Kerja, Family Gathering, Diklat yang di dalamnya terdapat m', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Dapat mengacu pada Keputusan Bersama Kapusbin dan Kapusdiklat JFA Nomor KEP-182/JF/1/2014 dan KEP-168/DL/2/2014 tanggal 3 Maret 2014 tentang Kurikulum Diklat JFA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Pedoman Tata Laksana Tim Audit (seperti: kebijakan tentang tata laksana tim audit, atau SOTK yang di dalamnya mengatur tata laksana tim audit)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Nota Dinas/Memo/SE Tentang penyelenggaraan komunikasi internal APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'Laporan Pelaksanaan PKS/PPM/ Workshop/ Pemagangan/ Join Audit / Ekspose Hasil Pengawasan dsb (laporan dilengkapi daftar hadir, jadwal kegiatan dan sertifikat â€”jika ada)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'Kebijakan Peningkatan Peran SDM dalam penugasan Assurance dan Consulting (seperti: Staf ditugaskan pada penugasan assurance di satu kesempatan dan consulting di kesempatan yang lain)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Kebijakan pemberian penghargaan terhadap tim audit berkinerja baik', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'Pelaksanaan Pemberian Penghargaan (SK Inspektur penetapan tim berkinerja baik, Piagam Penghargaan tim berkinerja baik)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, '\'Daftar Obyek Potensial Pengawasan (Audit universe)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, '\'Notulen konsultasi atau dokumen lain yang berisi masukan pimpinan Pemda', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'SOP penyusunan PKPT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'PKPT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'PKPT berikut Lampiran kertas kerjanya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'PKPT yang sudah ditandatangani Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'SPT Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'PKPT yang memuat Penugasan Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'IAC (memuat kewenangan mengakses informasi organisasi  tersebut), penetapan/pemberlakuan  Standar Audit,  Kode etik yang digunakan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'IAC yang sudah ditandatangani', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Pedoman/juklak tentang Kebijakan SDM, Manajemen Informasi dan Keuangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Pedoman/juklak pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Pedoman Kendali Mutu atau Pemberlakuan Permenpan 19/2009. Sampel  Surat Penugasan, PKA, KKA, ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Laporan,  sebagai bukti pendukung  penerapan  Standar Audit dan Kendali Mutu.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Notulensi PKS/PPM PPBR   dan atau Sertifikat mengikuti  Workhop/Bimtek PPBR dan atau Sertifikait  Diklat PPBR  dan atau sertifiktat CRMO/CRMP/CRMA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Kertas Kerja penilaian OPD ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Register Risiko', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Peta auditan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, '\'Kertas Kerja penyusunan PPBR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'PKPT Berbasis Risiko   Penugasan Audit ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Kinerja termasuk audit ketaatan yang didasarkan pada PPBR yang telah disusun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Kebijakan/pedoman/petunjuk QAIP mencakup: Reviu Berjenjang, Reviu Internal, Peer Review', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Routing Slip reviu berjenjang (Sample Surat Tugas,  PKA,  KKA, Simpulan dan Pelaporan) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'ST Reviu Internal, atau Laporan hasil reviu internal', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'ST Reviu Eksternal, atau Laporan hasil reviu eksternal', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, '\'Dokumen Bukti tindak lanjut Hasil Reviu internal dan Peer review', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Hasil wawancara kepada para pemangku kepentingan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'RKT/ Renja/DPA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'RKT/ Renja/DPA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'RKT/ Renja/DPA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Standar Satuan Harga (SSH), atau Standar Biaya Masukan (SBM), yang berlaku di Pemda, atau  Standar Biaya Khusus Operasional Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'DPA (sudah ditandatangani Kepala Daerah)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'Laporan Realisiasi Fisik dan Keuangan (Bulanan), atau Analisis atas Laporan Realisasi Fisik dan Keuangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Dokumen Permintaan Tambahan Anggaran atau dokumen revisi RKT atau revisi PKPT  atau revisi kegiatan  dukungan sebagai Tindak Lanjut atas  hasil Reviu Anggaran Operasional', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'Kebijakan dan Prosedur Pelaporan Pertanggung Jawaban Kinerja APIP (Peraturan Kepala Daerah tentang SOTK, Permen PAN RB Nomor 42 Tahun 2011 tentang Laporan,  Ikhtisar Hasil Pengawasan, Pemen PAN RB Nomor 53 Tahun 2014 tentang Laporan Kinerja Instansi Pemer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'e-SAKIP, atau SIM HP, atau SIM Monev, atau SIM Pengendalian Anggaran, atau SILABI, atau IPMS atau SIMDA serta aplikasi lain yang untuk berbagai  pelaporan kegiatan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'SOP/Pedoman Penyusunan Pelaporan Manajemen', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'LKjIP atau LAKIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'IHPS (Ikhtisar Hasil Pengawasan Semesteran)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Surat Pengantar  atau bukti lain Pengiriman LKjIP dan IHPS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'Dokumen Survei Kepuasan Stakeholders', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'Hasil Wawancara Tim QA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'Penggunaan   laporan APIP sebagai  dasar  penilaian atau pemberian Penghargaan SKPD Terbaik (Jika ada)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'SK/Kebijakan tentang Standar Biaya baik oleh Pemda ataupun oleh APIP sendiri dengan menetapkan sendiri  standar biaya intern  (jumlah orang hari)  untuk setiap jenis kegiatan pengawasan baik  dalam RKT  atau PKPT nya.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'SIMDA Keuangan, atau SIMDA Perencanaan, atau SIMONEV, atau SIM Pengendalian Anggaran, atau SIPKD, dsb.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'Dokumen Hasil Pemantauan Sistem Informasi Biaya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'Dokumen Revisi Anggaran, atau Dokumen Permintaan Penambahan Anggaran, atau Dokumen Analisis Penggunaan Anggaran atau dokumen revisi PKPT (misal: pengurangan jumlah orang atau jumlah hari atau perubahan jumlah PP  atau perubahan lokasi  dalam kegiatan peng', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'Dokumen indikator Kinerja Utama (IKU), dan Dokumen Perjanjian Kinerja (Perkin)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'Dokumen Perjanjian Kinerja (Perkin)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'SOP Sistem Manajemen Kinerja atau SAKIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'Laporan LKjIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'LAKIP/LKjIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'Bukti Pengiriman LAKIP/LKjIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'Laporan Berkala Capaian Kinerja ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'LAKIP/LKjIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'Laporan Berkala Capaian Kinerja ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'LAKIP/LKjIP termasuk informasi  rekomendasi / saran hasil pengawasan oleh APIP pada  tahun berjalan  yang telah di tindaklanjuti oleh unit kerja Pemda / OPD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'Peraturan Gubernur/ Bupati/Walikota tentang SOTK APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'Analisis Jabatan, atau SK Gubernur/ Bupati/Walikota tentang Uraian Tugas dan Jabatan di Lingkungan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'Informasi SDM, anggaran, termasuk perangkat berbasis teknologi informasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'Surat Permintaan Pemenuhan Kebutuhan SDM kepada BKD, atau Surat Permintaan Pemenuhan Kebutuhan anggaran kepada Kepala DPKAD, atau Surat Permintaan Pemenuhan Kebutuhan Sarpras kepada Sekda/Biro/BagianPerlengkapan, atau Surat Permintaan Pemenuhan Kebutuhan ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'ND/SE Inspektur tentang pelaksanaan komunikasi Internal (Misalnya: Forum, Apel Pagi, Rapat Berkala, pengarahan/briefing,  PPM, Ekspose Draft LHP dll), atau Surat Undangan/SPT/ND Pelaksanaan PPM/ Ekspose Draft Laporan, Notulensi dan Daftar Hadir', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'Undangan Rapat Kepada Pimpinan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'Daftar Hadir dan Notulen Rapat', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'SK Kepala Daerah tentang Pembentukan Tim atau Satgas yang melibatkan Staf APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'Laporan Kegiatan Satgas yang melibatkan Staf APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'Undangan/Surat Tugas Koordinasi, atau Berita Acara pembahasan TLRHP ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'RPJMD/Renstra', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'Laporan Keuangan OPD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'RKA SKPD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 'LAKIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'Surat permintaan/Nota Dinas/Disposisi atau dokumen lain  dari Kepala Daerah/Manajemen Pemda untuk melakukan pengawasan intern, dan atau notulen rapat pembahasan dengan Manajemen, dan atau SPT untuk melaksanakan pengawasan atas permintaan manajemen  serta ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'Nota Dinas/SE tentang Rapat Staf, Rapat Internal, Apel Pagi, Penyelenggaraan Komunikasi Internal APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 'Daftar Hadir', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'Notulen', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'IAC memuat koordinasi dengan pihak lain yang menyediakan jasa penjaminan dan advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'Pedoman/ SOP Koordinasi dengan pihak lain yang menyediakan jasa penjaminan dan advis, atau', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'SPT melaksanakan koordinasi, atau WA Group yang melibatkan APIP dengan penyedia jasa pemberian penjaminan dan saran lainnya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 'Undangan Rakorwasda/Rakorwasnas/Larwasda, atau SPT Menghadiri  Rakorwasda /Rakorwasnas/Larwasda berikut Daftar hadir Rakorwasda atau Notulen/Laporan Pelaksanaan Rakorwasda', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'Berita Acara atau notulen Kesepakatan rencana kerja pengawasan atau  PKPT atau penyesuaian Jadwal Pengawasan Intern ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'Peraturan tentang SOTK APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'IAC yang telah memuat visi, misi, tujuan, wewenang, dan tanggung jawab APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'Bukti Penyampaian IAC kepada seluruh Organisasi Pemda atau  Bukti upload IAC dalam website APIP atau bukti  sosialisasi IAC dalam gelarwasda, ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 'Undangan Gelarwasda, ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 'Daftar Hadir atau Notulen atau Laporan Kegiatan Larwasda', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 'IAC versi awal dan IAC versi terbaru bila ada perubahan atau Keterangan bahwa IAC masih relevan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 'SOP Pelaporan Administratif dan Hasil Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 'Laporan Ikhtisar Hasil Pengawasan kepada Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 'Bukti Kirim atau tanda terima  Laporan Hasil Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 'IAC yang memuat kewenangan APIP untuk mengakses seluruh informasi organisasi, aset dan SDM dalam tugas pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 'SOP/Pedoman pelaksanaan pengawasan mencakup prosedur permintaan data dan  prosedur yang dilakukan apabila auditan tidak bersedia memberikan akses secara penuh kepada Inspektorat dalam pelaksanaan tugas pengawasan, dan diuraikan dalam laporan bahwa pengawa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 'SOP//Pedoman pelaksanaan pengawasan  mencakup prosedur permintaan data dan  prosedur yang dilakukan apabila auditan tidak bersedia memberikan akses secara penuh kepada Inspektorat dalam pelaksanaan tugas pengawasan, dan diuraikan dalam laporan bahwa penga', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 'PKPT (Berbasis Risiko)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 'DPA (telah sesuai dengan PKPT)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 'SOP Pengajuan anggaran/ revisi anggaran dan persetujuannya ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 'SOP Revisi Anggaran dan analisis dampak keterbatasan/ pengurangan anggaran terhadap rencana pengawasan (misal: pengurangan jumlah  hari pengawasan, pengurangan jumlah orang, perubahan jumlah PP, perubahan  lingkup atau lokasi  pengawasan, revisi PKPT ke p', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 'IAC yang memuat peran jajaran Pimpinan Daerah mengawasi kinerja APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 'SK Kepala Daerah/ SK Tim Ad hoc/ Surat Tugas/ Nota Dinas/Disposisi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 'SOP Hubungan Kerja antara APIP dengan Tim Pengawas dan Pemberi Dukungan kepada APIP/SE/Nota Dinas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 'Surat Tugas/Undangan Tim Pengawas dan Pemberi Dukungan kepada APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 'Daftar Hadir Pertemuan Tim Pengawas dan Pemberi Dukungan kepada APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 'Notulen/Laporan Hasil Pertemuan Tim Pengawas dan Pemberi Dukungan kepada APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 'Tindak Lanjut Rekomendasi/Saran TimPengawas dan Pemberi Dukungan kepada APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kpa_organisasi`
--

CREATE TABLE `kpa_organisasi` (
  `id` int(11) NOT NULL,
  `organisasi_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `kpa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `kpa_organisasi`
--

INSERT INTO `kpa_organisasi` (`id`, `organisasi_id`, `element_id`, `level_id`, `kpa_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `nama_level` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `nama_level`, `created_at`, `updated_at`) VALUES
(1, 'Level 1', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(2, 'Level 2', '2019-10-01 17:00:00', '2019-10-01 17:00:00'),
(3, 'Level 3', '2019-10-01 17:00:00', '2019-10-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `organisasi`
--

CREATE TABLE `organisasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organisasi`
--

INSERT INTO `organisasi` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Pemerintah Provinsi Sulawesi Tengah', NULL, NULL),
(2, 'Pemerintah Kota Palu', NULL, NULL),
(3, 'Pemerintah Kabupaten Sigi', NULL, NULL),
(4, 'Pemerintah Kabupaten Donggala', NULL, NULL),
(5, 'Pemerintah Kabupaten Parigi Moutoung', NULL, NULL),
(6, 'Pemerintah Kabupaten Poso', NULL, NULL),
(7, 'Pemerintah Kab. Tojo Una-Una', NULL, NULL),
(8, 'Pemerintah Kab. Morowali', NULL, NULL),
(9, 'Pemerintah Kab. Morowali Utara', NULL, NULL),
(10, 'Pemerintah Kab. Banggai', NULL, NULL),
(11, 'Pemerintah Kab. Banggai Kepulauan', NULL, NULL),
(12, 'Pemerintah Kab. Banggai Laut', NULL, NULL),
(13, 'Pemerintah Kab. Toli-Toli', NULL, NULL),
(14, 'Pemerintah Kab. Buol', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_daftar_uji`
--

CREATE TABLE `ref_daftar_uji` (
  `id` int(11) NOT NULL,
  `nama_uraian` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ref_daftar_uji`
--

INSERT INTO `ref_daftar_uji` (`id`, `nama_uraian`, `created_at`, `updated_at`) VALUES
(1, 'APIP telah memiliki mandat untuk melaksanakan audit Ketaatan dan dituangkan dalam IAC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'APIP Telah mencantumkan audit Ketaatan dalam Perencanaan Pengawasan Tahunan (PKPT)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'APIP telah memiliki Pedoman / SOP terkait Pelaksanaan Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'APIP Telah melaksanakan Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'APIP telah Menyusun Laporan Hasil Audit Ketaatan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Hasil Audit Ketaatan Telah Ditindaklanjuti', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Pelaksanaan audit ketaatan telah memberikan outcome berupa peningkatan ketaatan terhadap peraturan dan pencegahan tindak penyimpangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'APIP telah memiliki mandat untuk melaksanakan Audit Kinerja dan kegiatan pengawasan lain (evaluasi, reviu, pemantauan)  yang dituangkan dalam IAC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'APIP telah mencantumkan Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan)  dalam Perencanaan Pengawasan Tahunan (PKPT)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'APIP telah memiliki Pedoman / SOP terkait pelaksanaan Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'APIP telah melaksanakan Audit Kinerja  dan kegiatan pengawasan lain (evaluasi, reviu, pemantauan) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'APIP telah Menyusun Laporan Hasil Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Hasil Audit Kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan)  telah ditindaklanjuti', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Pelaksanaan audit kinerja dan  kegiatan pengawasan lain (evaluasi, reviu, pemantauan) telah memberikan outcome berupa perbaikan efektivitas, efisiensi, ke-ekonomisan serta peningkatan kinerja serta peningkatan tata kelola, manajemen risiko dan pengendalian organisasi Pemerintah Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'APIP telah memiliki mandat untuk memberikan jasa advis dan dituangkan dalam IAC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'APIP telah mencantumkan Layanan Jasa Advis dalam Perencanaan Pengawasan Tahunan (PKPT)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'APIP telah memiliki Pedoman / SOP terkait Pelaksanaan Jasa Advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'APIP Telah melaksanakan Layanan Jasa Advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'APIP telah Menyusun Laporan Layanan Jasa Advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Hasil Layanan Jasa Advis Telah Memberikan Nilai Tambah Perbaikan GRC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'APIP telah megidentifikasi dan menentukan secara spesifik jenis pengawasan intern yang harus dilaksanakan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'APIP telah mengidentifikasi pengetahuan (knowledge), skill (teknis dan perilaku) serta kompetensi yang dibutuhkan untuk melaksanakan tugas pengawasan tersebut', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'APIP telah menyusun uraian jabatan serta klasifikasi tunjangan untuk setiap Jabatan di lingkungan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'APIP telah melaksanakan proses rekrutmen SDM yang transparan dan kredibel (Pengusulan rekrutmen CPNS, Pemenuhan tenaga fungsional dari unit lain, Pengangkatan perpindahan)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'APIP telah memiliki SDM yang kompeten untuk melaksanakan kegiatan pengawasan intern sehingga menghasilkan temuan dan rekomendasi yang bermutu', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'APIP telah menetapkan jam/hari/angka kredit pelatihan minimal bagi setiap tenaga fungsional pengawasan sesuai dengan standar audit, persyaratan sertifikasi profesi tertentu dan kebijakan organisasi.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'APIP telah mengidentifikasi kebutuhan pendidikan, pelatihan, penyelenggara diklat, dan pelatihan internal (PKS/PPM) yang dapat memenuhi pengembangan profesi individu tenaga fungsional pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'APIP telah mendorong SDM APIP untuk menjadi anggota organisasi profesi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'APIP telah memantau dan menyusun laporan periodik program pengembangan profesi untuk memastikan setiap SDM APIP memenuhi jam minimal pelatihan yang dipersyaratkan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'APIP telah mengestimasi jumlah dan ruang lingkup pengawasan intern yang diperlukan untuk melaksanakan rencana kerja pengawasan serta membandingkannya dengan jumlah dan keahlian SDM yang tersedia.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'APIP telah menyusun prioritas pengawasan berdasarkan kapasitas maksimal SDM Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'APIP telah menyusun kebijakan dan strategi pemenuhan SDM pengawasan (rekrutmen, co-sourcing dan outsourcing) sesuai kebutuhan), ketika kapasitas APIP tidak mencukupi untuk melaksanakan tugas pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'APIP telah menyusun kerangka kompetensi pegawai yang selaras dengan pola pengembangan karir dan kriteria penilaian kinerja SDM APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'APIP telah menyusun rencana pelatihan dan pengembangan SDM APIP yang berpedoman pada kerangka kompetensi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'APIP telah memiliki program  untuk perolehan sertifikasi penjenjangan jabatan fungsional auditor dan jabatan fungsional lain di bidang pengawasan intern pemerintah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'APIP telah memiliki program  untuk perolehan sertifikasi profesi internal auditor, misalnya QIA, CIA, CGAP, CFE, CRMP dan sebagainya.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Staf yang melaksanakan penugasan pengawasan telah memiliki sertifikasi JFA dan atau sertifikasi profesi internal auditor lain yang relevan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'APIP telah memiliki metode penilaian kinerja yang berpedoman pada kerangka kompetensi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'APIP telah menyusun pola karir didasarkan pada pola karir yang selaras dengan kinerja dan kompetensinya.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'APIP telah memberikan penghargaan kepada SDM yang berhasil mencapai target dan berkinerja baik', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'APIP telah membangun kriteria perilaku dan praktik kerja sama tim yang efektif;', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'APIP telah memiliki program pengembangan kompetensi dan pengembangan kepada pegawai terkait kerja sama tim, antara lain kepemimpinan, komunikasi yang efektif, dan membangun hubungan kerja yang baik', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'APIP telah menyusun pedoman tata laksana tim audit yang mengatur secara jelas tugas dan tanggung jawab, kewajiban dan kewenangan tim', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'APIP telah memiliki mekanisme komunikasi dan koordinasi tim audit', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'APIP telah memiliki mekanisme berbagi pengetahuan dan pengalaman', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Manajemen APIP telah mengembangkan setiap personal dalam tim untuk selalu siap berganti peran (assurance dan consulting) seiring dengan perubahan organisasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Manajemen APIP telah menyusun kebijakan dan menerapkan pemberian penghargaan atas keberhasilan tim dalam penugasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'APIP telah mengidentifikasi semua unit kerja/auditan yang dapat dijadikan sebagai sasaran audit atau audit universe (urusan, unit, program, kegiatan yang dapat diaudit), termasuk dokumen pendukungnya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'APIP telah meminta masukan kepada Manajemen/Pemangku Kepentingan atas kegiatan APIP yang menjadi proioritas dalam PKPT (Kepala Daerah, Sekda, OPD dsb.)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'PKPT telah mencantumkan sumber daya yang diperlukan (SDM, Waktu, Dana).', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'PKPT telah disetujui oleh Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'APIP melaksanakan pengawasan berdasarkan perencanaan (PKPT) ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'APIP telah memiliki kewenangan untuk mengakses catatan, personil, kekayaan fisik dan notulensi rapat terkait kegiatan pengawasan, penetapan/ pemberlakuan  Standar Audit,  Kode etik yang harus diterapkan, telah dinyatakan  dalam IAC', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Internal Audit Charter telah disetujui oleh Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'APIP telah memiliki kebijakan dalam bentuk peraturan/SK yang mendukung kegiatan pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'APIP telah memiliki pedoman/Juklak/SOP pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'APIP telah menerapkan standar audit dan kendali mutu pada kegiatan pengawaan yang dilaksanakan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'APIP telah melakukan pelatihan Perencanaan Pengawasan Berbasis Risiko', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'APIP mengidentifikasi OPD yang memiliki kontribusi/peran terbesar dalam pencapaian tujuan Pemda', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'APIP telah mendapatkan register risiko OPD tersebut. Apabila belum ada register risiko, APIP sebagai fasilitator bersama dengan manajemen mengidentifikasi dan melakukan penilaian risiko ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'APIP menggunakan register risiko OPD untuk digunakan untuk memutakhirkan peta auditan sebagai dasar penyusunan perencanaan pengawasan berbasis risiko', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'APIP menyusun  dan melaksanakan penugasan berdasarkan perencanaan pengawasan tahunan dengan mempertimbangkan risiko auditan dan kontribusi/peran auditan terhadap pencapaian tujuan Pemda.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'APIP telah merumuskan kebijakan tentang program penjaminan kualitas (QAIP) yaitu kebijakan on going monitoring (reviu berjenjang dan mekanisme persetujuan setiap tahap penugasan) serta penilaian mutu (quality assessment) secara periodik baik secara internal (reviu oleh tim internal  APIP maupun antar Irban), maupun eksternal (melalui peer review)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'APIP telah melaksanakan program penjaminan kualitas (QAIP) melalui on going monitoring(Reviu berjenjang)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'APIP telah melaksanakan  dan melaporkan kepada stakeholders   hasil program penjaminan kualitas (QAIP) melalui quality assessment secara periodik oleh pihak internal (reviu oleh tim internal APIP maupun reviu  antar Irban)  dan  oleh pihak eksternal (melalui peer revie) untuk menilai kesesuaian pelaksanaan kegiatan pengawasan intern dengan Kode Etik dan Standar Audit Intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Hasil Reviu internal (baik oleh tim internal APIP maupun reviu  antar Irban) dan Peer Review antar APIP telah ditindaklanjuti', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Hasil pengawasan APIP telah meningkatkan kepercayaan para pemangku kepentingan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'APIP telah memiliki Rencana Kinerja Tahunan (RKT) /Rencana Kerja (Renja) yang mengidentifikasi sasaran dan hasil yang ingin dicapai, termasuk indikator keberhasilannya (misal: rencana/target, input, output, outcome),  serta langkah -langkah untuk mencapai sasaran tersebut.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'RKT/Renja telah mempertimbangkan penetapan jadwal dan sumber daya yang dibutuhkan termasuk kegiatan administrasi dan dukungan pengawasan  yang diperlukan  (misal: Pelatihan SDM, Sarana Prasarana, Rencana Pengembangan Teknologi Informasi)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'RKT/Renja telah dikomunikasikan, memperoleh persetujuan dari Kepala Daerah serta memperoleh alokasi anggaran', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'APIP telah menyusun anggaran secara realistis/wajar untuk kegiatan-kegiatan dan sumber daya yang telah diidentifikasikan dalam RKT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'Anggaran operasional APIP telah disahkan oleh Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'APIP telah melakukan reviu terhadap anggaran operasional secara berkesinambungan untuk memastikan bahwa anggaran yang disusun masih realistis dan akurat, dan telah mengidentifikasi serta melaporkan setiap selisih yang ada.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'APIP telah mengidentifikasi Pelaporan Manajemen dan Pengawasan yang harus disusun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'APIP telah menggunakan sistem informasi pengumpulan dan pengolahan data yang relevan untuk tujuan pelaporan kegiatan pengawasan intern.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'APIP telah menyampaikan laporan seluruh hasil kegiatan pengawasan intern untuk memenuhi kebutuhan pengguna dan para pemangku kepentingan yang utama secara tepat waktu dan berkala', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'APIP telah melakukan pemantauan penggunaan laporan dan informasi untuk melihat apakah masih relevan, dan melakukan perbaikan apabila diperlukan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'APIP telah memiliki kebijakan tentang standar biaya untuk melaksanakan kegiatan pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'APIP telah menerapkan standar biaya untuk kegiatan pengawasan intern kedalam sistem informasi biaya dengan menggunakan teknologi informasi (aplikasi komputer dan hardware).', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'APIP telah memantau sistem informasi biaya secara berkala dan memastikan bahwa biaya kegiatan pengawasan intern tersebut telah  efisien dan ekonomis.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'APIP telah memanfaatkan hasil pemantauan biaya kegiatan pengawasan intern untuk pengambilan keputusan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'APIP telah menetapkan kegiatan pengawasan intern yang perlu diukur kinerjanya dan menetapkan target kinerjanya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'APIP telah mengembangkan ukuran-ukuran kinerja (rasio input/output, indikator produktivitas)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'APIP telah mendokumentasikan Sistem Manajemen Kinerja dalam pengelolaan kegiatan pengawasan intern, yang mencakup: identifikasi data kinerja yang harus dikumpulkan, frekuensi pengumpulan data, siapa yang bertanggung jawab untuk pengumpulan data, pengendalian mutu data, siapa yang menghasilkan laporan data kinerja, dan siapa yang menerima laporan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'APIP telah melaporkan kinerja kegiatan pengawasan intern kapan saja dibutuhkan oleh Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'APIP telah melakukan evaluasi secara berkala terhadap efektivitas biaya dan relevansinya dengan ukuran-ukuran kinerja', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'APIP telah menggunakan informasi kinerja untuk memantau kegiatan pengawasan intern dan hasilnya dibandingkan dengan tujuan yang telah  di tetapkan serta kontibusinya terhadap pencapaian tujuan Pemda.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'APIP telah memiliki struktur organisasi yang tepat untuk melaksanakan pengawasan intern (sesuai dengan kebutuhan dan budaya organisasi Pemda)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'APIP telah mengidentifikasi peran dan tanggung jawab seluruh jabatan struktural di lingkungan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'APIP telah mengidentifikasi kebutuhan sumber daya yang diperlukan untuk melaksanakan pengawasan intern (informasi, SDM, anggaran, termasuk perangkat berbasis teknologi informasi), dan upaya untuk memperolehnya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'APIP telah mengembangkan komunikasi intern dan membangun hubungan pelaporan intern kegiatan APIP yang efektif, antara lain pembentukan forum, Apel Pagi, PPM, ekspose rutin hasil pengawasan, rapat internal dsb.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Pimpinan APIP berpartisipasi dan berkontribusi dalam komite/forum/rapat/pertemuan  Manajemen', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Pimpinan APIP mendorong pegawai untuk aktif dalam komite/forum organisasi Pemerintah Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'APIP menjadi penghubung organisasi Pemda (sebagai wakil manajemen) dalam berkoordinasi dengan auditor eksternal (Koordinasi Jadwal Pemeriksaan, pendampingan Pelaksanaan Pemeriksaan dan Pembahasan Undangan Rapat Pembahasan Tindak Lanjut Rekomendasi Hasil Pemeriksaan /TLRHP)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Manajemen Pemda menyampaikan rencana organisasi dan informasi penting kepada Pimpinan APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'APIP telah mempertimbangkan masukan Manajemen dalam pengembangan rencana kegiatan pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Pimpinan APIP menginformasikan dan mendiskusikan rencana dan isu-isu organisasi Pemda kepada seluruh Staf', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'APIP telah memiliki mandat untuk berkoordinasi dengan pihak lain yang menyediakan jasa penjaminan dan advis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'APIP mengembangkan proses atau mekanisme komunikasi, koordinasi dan berbagi informasi dengan unit penyedia jasa pemberian penjaminan dan saran lainnya untuk meminimalisir duplikasi pengawasan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'APIP secara reguler berkomunikasi, berbagi informasi, saling mendukung dan menjadi penghubung antara organisasi Pemda dengan unit penyediaan jasa  pemberian penjaminan dan saran lainnya (BPK, BPKP, APIP lainnya).', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Kegiatan Koordinasi memberikan outcome berupa berkurangnya duplikasi pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Visi, misi, tujuan, wewenang, dan tanggung jawab unit APIP telah mendefinisikan secara formal ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Tujuan, wewenang, dan tanggung jawab unit APIP telah dikomunikasikan ke seluruh organisasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'APIP telah melakukan reviu dan memutakhirkan IAC secara berkala dan mendapat persetujuan dari Kepala Daerah.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'APIP telah memastikan bahwa laporan administratif dan fungsional APIP telah ditujukan kepada Kepala Daerah dan disampaikan tepat waktu', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'APIP memiliki kewenangan untuk akses ke seluruh informasi organisasi, aset dan SDM dalam tugas pengawasan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Terdapat prosedur tindak lanjut, apabila auditan tidak bersedia memberikan akses secara penuh kepada APIP dalam pelaksanaan tugas pengawasan.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'Terdapat prosedur tindak lanjut, apabila Kepala Daerah melakukan intervensi/membatasi akses secara penuh kepada Inspektorat dalam pelaksanaan tugas pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Anggaran pengawasan telah memperhatikan besaran sumber daya yang dibutuhkan untuk melaksanakan kegiatan prioritas/ berbasis risiko', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'APIP memiliki prosedur pengajuan anggaran dan proses persetujuannya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'APIP telah memiliki prosedur untuk melakukan revisi anggaran, termasuk menginformasikan dampak pengurangan anggaran terhadap rencana kegiatan pengawasan kepada Kepala Daerah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'Adanya mandat mengenai peran jajaran pimpinan Daerah dalam membimbing, membina dan mengawasi kinerja APIP ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'Penetapan Tim Pengawas dan Pemberi dukungan kepada APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'Terdapat SOP tentang  hubungan kerja antara APIP dengan Tim Pengawas dan Pemberi dukungan kepada APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Tim Pengawas dan Pemberi dukungan kepada APIP telah melaksanakan tugasnya untuk mendukung dan mengawasi kinerja APIP', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ref_kpa`
--

CREATE TABLE `ref_kpa` (
  `id` int(11) NOT NULL,
  `judul_kpa` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ref_kpa`
--

INSERT INTO `ref_kpa` (`id`, `judul_kpa`, `created_at`, `updated_at`) VALUES
(2, 'APIP melaksanakan value for money audit/program evaluasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'APIP memberikan jasa advis (advisory services)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'APIP mengidenti fikasi dan merekrut SDM yang kompeten', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'APIP telah melakukan pengembangan profesi bagi individu audito', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Adanya koordinasi SDM APIP (workforce coordination)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Tersedianya staf APIP yang berkualifikasi profesional', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Adanya kompetensi dan team building', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Perencanaan pengawasan disusun berdasarkan pada prioritas manajemen/ pemangku kepentingan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'APIP memiliki kerangka kerja praktik profesional berikut prosesnya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Adanya perencanaan pengawasan berbasis risiko', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'APIP memiliki kerangka kerja mengelola kualitas ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Adanya Perencanaan kegiatan Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Adanya Anggaran Operasional Kegiatan Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Adanya Laporan Manajemen Kegiatan Pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Adanya Informasi Mengenai Biaya', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Adanya Sistem Pengukuran Kinerja', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Adanya Pengelolaan atas proses bisnis pengawasan intern', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'APIP bersama sama dengan unit lain dalam organisasi merupakan satu tim manajemen', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Koordinasi dengan pihak lain yang memberikan saran dan penjaminan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Hubungan pelaporan telah terbangun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Akses penuh terhadap informasi organisasi, aset dan SDM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Adanya Mekanisme pendanaan kegiatan pengawasan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Adanya pengawasan terhadap pelaksanaan kegiatan APIP oleh manajemen organisasi Pemda', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_uji_kpa_organisasi`
--
ALTER TABLE `daftar_uji_kpa_organisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_daftar_uji_kpa_organisasi`
--
ALTER TABLE `item_daftar_uji_kpa_organisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_dokumen_daftar_uji`
--
ALTER TABLE `item_dokumen_daftar_uji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpa_organisasi`
--
ALTER TABLE `kpa_organisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisasi`
--
ALTER TABLE `organisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_daftar_uji`
--
ALTER TABLE `ref_daftar_uji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_kpa`
--
ALTER TABLE `ref_kpa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftar_uji_kpa_organisasi`
--
ALTER TABLE `daftar_uji_kpa_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item_daftar_uji_kpa_organisasi`
--
ALTER TABLE `item_daftar_uji_kpa_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `item_dokumen_daftar_uji`
--
ALTER TABLE `item_dokumen_daftar_uji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `kpa_organisasi`
--
ALTER TABLE `kpa_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `organisasi`
--
ALTER TABLE `organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ref_daftar_uji`
--
ALTER TABLE `ref_daftar_uji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `ref_kpa`
--
ALTER TABLE `ref_kpa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
